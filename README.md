# LIS5364 Database Presentation
## Snips folder
* contains screenshots that were used in the presentation
## Images folder
* contains images of different DBMS UI's that were used in the presentation
## References
* contains pdf's of three articles that were used in the presentation, these articles can be found online at:
	* http://www.seas.upenn.edu/~zives/03f/cis550/codd.pdf
	* http://www.cs.berkeley.edu/~brewer/cs262/SystemR-comments.pdf
	* http://jpkc.ecnu.edu.cn/0803/Readings/FS76.pdf
### .pptx file of the presentation
### .docx file of speaking notes for the presentation


